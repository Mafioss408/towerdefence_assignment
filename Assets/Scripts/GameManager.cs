using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour

{
    [Header("WorldReference")]
    public GameObject platform;
    [SerializeField] public LayerMask turretMask;
    [SerializeField] public LayerMask enemyMask;


    [Header("Wave Setting")]
    [SerializeField] private int wave = 1;
    [SerializeField] private TextMeshProUGUI WaveText;
    [SerializeField] private GameObject waitWave;
    [SerializeField] private float timePreWave = 10f;
    [SerializeField] private int enemyToFinishWave;

    [Header("Spawn Enemy")]
    [SerializeField] private GameObject enemySpawn;


    [Header("Setting Button")]

    [SerializeField] private TextMeshProUGUI pistolText;
    //--------
    [SerializeField] private TextMeshProUGUI minigunText;
    //--------
    [SerializeField] private TextMeshProUGUI cannonText;
    //--------
    [SerializeField] private TextMeshProUGUI buffRangeText;
    //--------
    [SerializeField] private TextMeshProUGUI buffDamageText;
    //-------- 
    [SerializeField] private TextMeshProUGUI buffRateoText;


    [SerializeField] private int[] buttonQty = new int[6];
    [SerializeField] private Button[] buttonActive;


    #region bool Variable for loop
    private bool startWave = true;
    private bool start = false;
    private bool incrementWave = false;
    private bool clearWave = false;
    private bool clearBase = false;
    private bool resetButtonQty = true;
    private bool addTurret = false;
    #endregion

    #region Property
    public float TimerPreWave { get { return timePreWave; } set { timePreWave = value; } }
    public float TimePreWave { get { return timePreWave; } set { timePreWave = value; } }

    public bool ClearBase { get { return clearBase; } set { clearBase = value; } }
    public bool ClearWave { get { return clearWave; } set { clearWave = value; } }
    public bool StartWave { get { return startWave; } set { startWave = value; } }
    public bool AddTurret { get { return addTurret; } set { addTurret = value; } }
    public int EnemyToFinishWave { get { return enemyToFinishWave; } set { enemyToFinishWave = value; } }
    public int[] ButtonQty { get { return buttonQty; } set { buttonQty = value; } }
    #endregion

    #region Singleton
    private static GameManager instance;

    public static GameManager Instance
    {
        get { return instance; }
        private set { instance = value; }
    }


    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(this);

    }


    public SpawnEnemy spawn { get; set; }
    public PlayerController playerRef { get; set; }
    public Turret turretRef { get; set; }
    public TurretBuff buff { get; set; }
    public Base Base { get; set; }
    public Shoot shoot { get; set; }
    public Enemy enemy { get; set; }
    public WaitWave WaitWave { get; set; }
    public DoorLoose DoorLoose { get; set; }
    #endregion

    private void Update()
    {
        WaveText.text = " Wave : " + wave;

        if (addTurret)
        {
            CountTurret();

        }

        Loop();
        SettingButtonQty();
        IncrementWave();
        ButtonOff();

    }

    public void TakeMono(MonoBehaviour turret)
    {
        if (playerRef.AddFeat.Count != 1)
        {
            playerRef.AddFeat.Add(turret);

        }
    }
    public void TakeGame(GameObject turret)
    {
        if (playerRef.AddFeatGameObject.Count != 1)
        {
            playerRef.AddFeatGameObject.Add(turret);

        }
    }
    public void AddBuff(MonoBehaviour buff)
    {
        if (playerRef.AddBuff.Count != 1)
        {
            playerRef.AddBuff.Add(buff);

        }
    }

    private void SettingTurret()
    {
        waitWave.SetActive(true);
    }

    private void WaveCountDown()
    {
        if (startWave)
        {
            clearBase = false;
            clearWave = false;
            SettingTurret();


            if (timePreWave <= 0)
            {
                waitWave.SetActive(false);
                TimePreWave = 10f;
                enemySpawn.SetActive(true);
                startWave = false;
            }
        }
    }

    private void Round()
    {
        if (start)
        {
            if (enemyToFinishWave == 0 && FindObjectOfType<DoorLoose>().Health > 0)
            {

                resetButtonQty = true;
                enemySpawn.SetActive(false);
                clearBase = true;
                clearWave = true;
                startWave = true;

                Instance.spawn.EnemyOnMap = 0;
                Instance.spawn.UpdateEnemyWave = true;

                incrementWave = true;

                addTurret = true;
                start = false;

            }
        }
    }

    private void Loop()
    {
        WaveCountDown();

        if (enemyToFinishWave == 1)
        {
            start = true;
        }

        Round();

    }

    private void IncrementWave()
    {
        if (incrementWave)
        {
            wave++;
            incrementWave = false;
        }
    }
    private void SettingButtonQty()
    {

        pistolText.text = buttonQty[0].ToString();
        minigunText.text = buttonQty[1].ToString();
        cannonText.text = buttonQty[2].ToString();
        buffRangeText.text = buttonQty[3].ToString();
        buffDamageText.text = buttonQty[4].ToString();
        buffRateoText.text = buttonQty[5].ToString();

    }

    private void ButtonActive()
    {
        for (int i = 0; i < buttonQty.Length; i++)
        {
            if (buttonQty[i] == 0)
            {
                buttonActive[i].enabled = false;
            }
            else if (buttonQty[i] > 0)
            {
                buttonActive[i].enabled = true;
            }
        }
    }


    private void CountTurret()
    {
        ResetButtonQty();

        if (!resetButtonQty)
        {
            for (int i = 1; i <= wave; i++)
            {
                if (i % 2 == 0)
                {
                    int tmp = Random.Range(3, 6);
                    buttonQty[tmp]++;
                }
                if (i % 2 != 0)
                {
                    int tmp = Random.Range(0, 3);
                    buttonQty[tmp]++;

                }
            }
            addTurret = false;
        }
    }


    private void ButtonOff()
    {
        if (Instance.playerRef.AddFeat.Count > 0)
        {
            foreach (var item in buttonActive)
            {
                item.enabled = false;
            }
        }
        else
        {
            ButtonActive();
        }
    }
    public void Click(int index)
    {
        buttonQty[index]--;
    }

    private void ResetButtonQty()
    {
        if (resetButtonQty)
        {
            for (int i = 0; i < buttonQty.Length - 1; i++)
            {
                buttonQty[i] = 0;
            }
            resetButtonQty = false;
        }
    }


    public void ChangeScene(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
