using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTurret : SettingTurret
{
    [Header("Variable")]
    [SerializeField] protected float speedRot;
    [SerializeField] protected List<GameObject> enemy = new List<GameObject>();

    [Header("Reference")]
    [SerializeField] private Transform muzzleRot;



    //private
    private int index = 0;


    void Update()
    {
        if (enemy.Count >= 1)
        {
            AttackTurret(muzzleRot.gameObject);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
            enemy.Add(other.gameObject);

    }
 
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {

            enemy.Remove(other.gameObject);
            index = 0;  

        }
    }

    protected override void AttackTurret(GameObject pos)
    {

        if (enemy[index].GetComponent<Enemy>().gameObject.activeSelf)
        {
            pos.transform.rotation = Quaternion.LookRotation(enemy[index].transform.position - pos.transform.position, Vector3.up * speedRot * Time.deltaTime);
            base.AttackTurret(pos);

        }
        else if (!enemy[index].GetComponent<Enemy>().gameObject.activeSelf)
        {
            if (index < enemy.Count - 1)
            {
                index++;

            }
            else
            {
                enemy.Clear();
                index = 0;

            }
        }
    }
}


