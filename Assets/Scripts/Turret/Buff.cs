using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Buff : MonoBehaviour
{


    [SerializeField] protected List<GameObject> turretBuffate = new List<GameObject>();



    protected virtual void Update()
    {
        DeBuff();
    }

    protected virtual void TrackBuff()
    {

        if (gameObject.TryGetComponent(out Base _base))
        {
            for (int i = 0; i < _base.Turret.Count; i++)
            {
                if (_base.Turret[i].GetComponent<Turret>())
                {
                    turretBuffate.Add(_base.Turret[i]);
                }
            }
        }
    }

    protected virtual void DeBuff()
    {
        if (gameObject.TryGetComponent(out Base _base))
        {

            // case damage
            if (FindObjectOfType<PlayerController>().Damage && _base.TryGetComponent(out MoreDamage damage))
            {
                TrackBuff();

                for (int i = 0; i < damage.turretBuffate.Count; i++)
                {
                    if (damage.turretBuffate[i] != null)
                        damage.turretBuffate[i].GetComponent<Shoot>().Damage -= 10;
                }
                FindObjectOfType<PlayerController>().Damage = false;
            }

            // case rateo
            if (FindObjectOfType<PlayerController>().Rateo && _base.TryGetComponent(out MoreRateo rateo))
            {
                TrackBuff();
                for (int i = 0; i < rateo.turretBuffate.Count; i++)
                {
                    if (turretBuffate[i] != null)
                        rateo.turretBuffate[i].GetComponent<Shoot>().RateoFire += 0.1f;
                }
                FindObjectOfType<PlayerController>().Rateo = false;
            }


            // case range
            if (FindObjectOfType<PlayerController>().Range && _base.TryGetComponent(out MoreRange range))
            {
                TrackBuff();

                for (int i = 0; i < range.turretBuffate.Count; i++)
                {
                    if (turretBuffate[i] != null)
                        range.turretBuffate[i].GetComponent<SphereCollider>().radius = 13f;
                }
                FindObjectOfType<PlayerController>().Range = false;
            }

        }
    }


}
