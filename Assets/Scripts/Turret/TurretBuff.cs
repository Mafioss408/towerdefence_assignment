
public class TurretBuff : SettingTurret
{

    private void Start()
    {
        GameManager.Instance.buff = this;

        if (GameManager.Instance.playerRef.AddFeatGameObject.Count > 0)
        {
            if (gameObject.TryGetComponent(out Base newBase))
            {
                if (newBase.Turret.Count > 0)
                {
                    base.AddTurret(GameManager.Instance.playerRef.AddFeatGameObject[0], transform.position);
                }
                else
                {
                    GameManager.Instance.playerRef.AddFeatGameObject.Clear();
                }
            }
        }
    }

}
