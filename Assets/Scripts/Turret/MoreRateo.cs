using UnityEngine;

public class MoreRateo : Buff
{
    [SerializeField] private float boostDamage = 0.1f;

    private void Start()
    {
        AddRateo();
    }
    

    private void AddRateo()
    {
        if (gameObject.TryGetComponent(out Base component))
        {

            foreach (var item in component.Turret)
            {

                if (item.gameObject.TryGetComponent(out Shoot shoot))
                {
                    shoot.RateoFire -= boostDamage;
                }

            }
        }


    }
}
