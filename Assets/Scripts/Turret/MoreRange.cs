using UnityEngine;

public class MoreRange : Buff
{

    private void Start()
    {
        AddRange();
    }

    private void AddRange()
    {
        if (gameObject.TryGetComponent(out Base component))
        {

            foreach (var item in component.Turret )
            {
                
                item.gameObject.GetComponent<SphereCollider>().radius = 20;


            }
        }


    }

}
