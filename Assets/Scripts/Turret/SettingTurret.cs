using UnityEngine;

public abstract class SettingTurret : MonoBehaviour
{

    protected virtual void AddTurret(GameObject turret, Vector3 pos)
    {
        if (gameObject.TryGetComponent(out Base newBase))
        {
            GameObject tmp = Instantiate(turret, pos, Quaternion.identity);
            GameManager.Instance.playerRef.AddFeatGameObject.RemoveAt(0);


            if (newBase.Turret.Count < newBase.LimitTurret)
            {

                newBase.Turret.Add(tmp);

                if (newBase.Turret.Count == 1)
                {
                    tmp.transform.position = new Vector3(pos.x, pos.y + 2.5f, pos.z);

                }
                else if (newBase.Turret.Count != 1)
                {
                    tmp.transform.position = new Vector3(pos.x, pos.y + newBase.Turret.Count, pos.z);
                }

            }
        }
    }



    protected virtual void AttackTurret(GameObject pos)
    {
        GetComponent<Shoot>().Shootpass();
    }

}
