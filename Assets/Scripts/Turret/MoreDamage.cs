using UnityEngine;


public class MoreDamage : Buff
{
    [SerializeField] private float boostDamage = 10;

    private void Start()
    {
        AddDamage();
    }


    private void AddDamage()
    {
        if (gameObject.TryGetComponent(out Base component))
        {

            foreach (var item in component.Turret)
            {

                if (item.gameObject.TryGetComponent(out Shoot shoot))
                {
                    shoot.Damage += boostDamage;
                }


            }
        }


    }
}
