using System.Collections.Generic;
using UnityEngine;


public class Base : MonoBehaviour
{
    [Header("Variable")]
    [SerializeField] private List<GameObject> turret = new List<GameObject>();
    [SerializeField] private int limitTurret;


    public List<GameObject> Turret { get { return turret; } set { value = turret; } }
    public int LimitTurret { get { return limitTurret; } }


    private void Start()
    {
        GameManager.Instance.Base = this;

    }

    private void Update()
    {


        if (GameManager.Instance.ClearWave)
        {
            ClearWave();

        }


        if (GameManager.Instance.ClearBase)
        {
            ClearBase();

        }

        for (int i = 0; i < turret.Count; i++)
        {

            if (turret[i] == null)
            {
                turret.RemoveAt(i);
                Destroy(GetComponent<Turret>());
            }
        }

        TurretReorder();
        ClearBuff();
    }

    private void TurretReorder()
    {

        for (int i = 0; i < turret.Count; i++)
        {
            turret[i].transform.position = new Vector3(turret[i].transform.position.x, i + 2.6f, turret[i].transform.position.z);
        }

    }

    private void ClearWave()
    {
        foreach (var item in turret)
        {
            Destroy(item);
        }
        turret.Clear();
    }
    


    private void ClearBase()
    {
        if (gameObject.GetComponent<Base>().turret.Count == 0)
        {
            foreach (var item in gameObject.GetComponents<Component>())
            {
                if (item != GetComponent<Base>() && item != GetComponent<BoxCollider>() && item != GetComponent<Transform>())
                {
                    Destroy(item);
                }
            }
        }
    }


    private void ClearBuff()
    {
        if (turret.Count > 0)
        {
            if (!turret[0].gameObject.GetComponent<Turret>())
            {

                for (int i = 0; i < turret.Count; i++)
                {
                    if (turret[i].gameObject.TryGetComponent(out TurretBuff buff))
                    {
                        for (int j = 0; j < GameManager.Instance.playerRef.TagsTurret.Length; j++)
                        {

                            if (buff.CompareTag(GameManager.Instance.playerRef.TagsTurret[j]))
                            {
                                GameManager.Instance.ButtonQty[j]++;

                            }

                        }

                        Destroy(turret[i]);
                        turret.Remove(turret[i].gameObject);
                    }
                }

            }

        }
    }


}
