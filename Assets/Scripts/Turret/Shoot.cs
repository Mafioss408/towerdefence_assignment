using UnityEngine;

public class Shoot : MonoBehaviour
{
    [Header("Reference")]
    [SerializeField] GameObject bulletPrefabs;
    [SerializeField] Transform muzzle;

    [Header("Shoot Stats")]
    [SerializeField] private float damage;
    [SerializeField] private float rateoFire;

    public float Damage { get { return damage; } set { damage = value; } }
    public float RateoFire { get { return rateoFire; } set { rateoFire = value; } }


    //private

    private float tmp = 0;

    private void Start()
    {
        GameManager.Instance.shoot = this;

    }


    private void ShootTurret(GameObject bullet)
    {

        GameObject projectile = Instantiate(bullet, muzzle.transform.position, muzzle.transform.rotation);
        projectile.transform.forward = muzzle.transform.forward;
        projectile.transform.parent = transform;
        
    }

    public void Shootpass()
    {
        tmp += Time.deltaTime;

        if (tmp > rateoFire)
        {
            ShootTurret(bulletPrefabs);
            tmp = 0;
        }

    }


}
