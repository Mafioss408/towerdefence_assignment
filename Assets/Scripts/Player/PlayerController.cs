using System;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MouseInput
{


    [SerializeField] private LayerMask postation;
    [SerializeField] private LayerMask RemuveTurretMask;


    [SerializeField] private List<MonoBehaviour> addFeat = new List<MonoBehaviour>();
    [SerializeField] private List<GameObject> addFeatGameObject = new List<GameObject>();
    [SerializeField] private List<MonoBehaviour> addBuff = new List<MonoBehaviour>();
    [SerializeField] private string[] tagsTurret = new string[6];



    // private 
    private bool damage;
    private bool range;
    private bool rateo;

    #region Property
    public List<MonoBehaviour> AddFeat { get { return addFeat; } set { addFeat = value; } }
    public List<GameObject> AddFeatGameObject { get { return addFeatGameObject; } set { addFeatGameObject = value; } }
    public List<MonoBehaviour> AddBuff { get { return addBuff; } set { addBuff = value; } }
    public string[] TagsTurret { get { return tagsTurret; } set { tagsTurret = value; } }
    public bool Damage { get { return damage; } set { damage = value; } }
    public bool Range { get { return range; } set { range = value; } }
    public bool Rateo { get { return rateo; } set { rateo = value; } }
    #endregion



    private void Start()
    {
        GameManager.Instance.playerRef = this;
    }

    private void Update()
    {
        try
        {
            RemuveTurret();

            if (addFeat.Count > 0)
            {
                AddFeature(addFeat[0]);
                RemuveFeature();
            }
            if (addBuff.Count > 0)
            {
                AddFeature(addBuff[0]);
                RemuveBuff();
            }
        }
        catch (NullReferenceException)
        {

        }
    }

    private void AddFeature(MonoBehaviour mono)
    {
        RaycastHit hit = base.mouseSetting(postation);

        if (Input.GetMouseButtonDown(0) && hit.collider.TryGetComponent(out Base b))
        {

            hit.collider.gameObject.AddComponent(mono.GetType());

            if (mono.GetComponent<TurretBuff>() && hit.collider.gameObject.GetComponent<Base>().Turret.Count == 0)
            {
                for (int i = 0; i < tagsTurret.Length; i++)
                {
                    if (AddFeat[0].gameObject.CompareTag(tagsTurret[i]))
                    {
                        GameManager.Instance.ButtonQty[i]++;

                    }
                }
            }
        }
    }


    private void RemuveFeature()
    {

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit = base.mouseSetting(postation);

            if (hit.collider.gameObject.GetComponent<Base>())
            {
                addFeat.RemoveAt(0);

            }

        }
    }

    private void RemuveBuff()
    {

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit = base.mouseSetting(postation);

            if (hit.collider.gameObject.GetComponent<Base>())
            {
                addBuff.RemoveAt(0);
            }
        }
    }

    private void RemuveTurret()
    {

        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit = base.mouseSetting(RemuveTurretMask);

            if (hit.collider.gameObject.GetComponent<RemuveTurret>())
            {

                if (hit.collider.gameObject.CompareTag(tagsTurret[3]))
                {
                    range = true;
                }

                if (hit.collider.gameObject.CompareTag(tagsTurret[4]))
                {

                    damage = true;
                }

                if (hit.collider.gameObject.CompareTag(tagsTurret[5]))
                {
                    rateo = true;
                }

                ReAddTurret(hit);
                Destroy(hit.transform.gameObject);
            }
        }
    }

    private void ReAddTurret(RaycastHit hit)
    {
        for (int i = 0; i < tagsTurret.Length; i++)
        {
            if (hit.collider.CompareTag(tagsTurret[i]))
            {
                GameManager.Instance.ButtonQty[i]++;
            }
        }
    }

}
