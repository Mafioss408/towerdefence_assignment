using UnityEngine;

public abstract class MouseInput : MonoBehaviour
{
    private Vector3 screenPos;

    protected virtual RaycastHit mouseSetting(LayerMask mask)
    {
        screenPos = Input.mousePosition;
        Ray ray = Camera.main.ScreenPointToRay(screenPos);


        Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, mask);
        return hit;
    }
}
