using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = " ScriptableObject", menuName = " ScriptableObject/Enemy")]
public class EnemyStats : ScriptableObject
{
    [SerializeField] private float health;
    [SerializeField] private float maxHealth;
    [SerializeField] private float speed;
    [SerializeField] private int damage;


    #region Property
    public float Health { get { return health; } set { health = value; } }
    public float MaxHealth { get { return maxHealth; } set { maxHealth = value; } }
    public float Speed { get { return speed; } set {  speed = value; } }
    public int Damage { get { return damage; } set { damage = value;  } }
    #endregion

}
