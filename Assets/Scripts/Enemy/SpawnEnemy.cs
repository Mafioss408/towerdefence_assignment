using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{

    [Header("Reference")]
    [SerializeField] private GameObject m_Enemy;

    [Header("Variable")]
    [SerializeField] private float frequency;
    [SerializeField] private int maxEnemy;


    //private
    private int enemyOnMap = 0;
    private float timer = 0;
    private bool updateEnemyWave = true;

    #region Property
    public float Frequency { get { return frequency; } set { frequency = value; } }
    public int MaxEnemy { get { return maxEnemy; } set { maxEnemy = value; } }
    public int EnemyOnMap { get { return enemyOnMap; } set { enemyOnMap = value; } }
    public bool UpdateEnemyWave { get { return updateEnemyWave; } set { updateEnemyWave = value; } }
    #endregion

    private void Start()
    {
        GameManager.Instance.spawn = this;
    }


    private void Update()
    {
        if (updateEnemyWave)
        {
            maxEnemy += 2;
            frequency -= 0.2f;
            GameManager.Instance.EnemyToFinishWave = maxEnemy;
            updateEnemyWave = false;
        }


        timer += Time.deltaTime;


        if (enemyOnMap < maxEnemy)
        {
            Timer();
        }

    }


    private void SpawnEn(GameObject enemy)
    {
        GameObject spawn = Instantiate(enemy, gameObject.transform.position, enemy.transform.rotation);
        enemyOnMap++;
    }

    private void Timer()
    {
        if (timer > frequency)
        {
            SpawnEn(m_Enemy);
            timer = 0;
        }
    }


}
