using System;
using UnityEngine;

[RequireComponent(typeof(EnemyPatrol))]
public class Enemy : MonoBehaviour, TakeDamage
{
    private EnemyPatrol patrol;
    private HPBar hP;

    [Header("ScriptableObject")]
    [SerializeField] private EnemyStats stats;

    [Header("Variable")]
    [SerializeField] private float health;
    [SerializeField] private float maxHealth;
    [SerializeField] private float damage;

    public float Health { get => health; set => value = health; }
    public float MaxHealth { get => maxHealth; set => value = maxHealth; }

    void Start()
    {
      
        GameManager.Instance.enemy = this;


        hP = GetComponentInChildren<HPBar>();
        patrol = GetComponent<EnemyPatrol>();
        SetUpStats();
        health = maxHealth;
    }

    private void Update()
    {

        hP.HpBar(health / maxHealth);

    }
    private void SetUpStats()
    {
        try
        {
            health = stats.Health;
            patrol.Nav.speed = stats.Speed;
            damage = stats.Damage;
            maxHealth = stats.MaxHealth;
        }
        catch (NullReferenceException)
        {

        }

    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.TryGetComponent<DoorLoose>(out DoorLoose doorLoose))
        {
            doorLoose.TakeDamage(damage);

            GameManager.Instance.EnemyToFinishWave--;
            gameObject.SetActive(false);
        }
    }
  
    public void TakeDamage(float damage)
    {
        health -= damage;

        if (health <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        GameManager.Instance.EnemyToFinishWave--;
        gameObject.SetActive(false);
    }
}

