using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolPoint : MonoBehaviour
{

    [SerializeField] private List<Transform> point = new List<Transform>();

    public List<Transform> Point { get { return point; } }

}
