using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyPatrol : MonoBehaviour
{
    private PatrolPoint patrol;
    private NavMeshAgent nav;
    private int index = 0;
    private Vector3 target;

    public NavMeshAgent Nav { get { return nav; } set { nav = value; } }

    [SerializeField] private List<Transform> patrolPoint = new List<Transform>();

    private void Awake()
    {
        nav = GetComponent<NavMeshAgent>();
    }
    void Start()
    {
        patrol = FindObjectOfType<PatrolPoint>();

        foreach (var item in patrol.Point)
        {
            patrolPoint.Add(item);
        }
        FollowPoint();
    }


    void Update()
    {
        if (Vector3.Distance(transform.position, target) < 1)
        {
            incrementPoint();
            FollowPoint();

        }

    }

    private void FollowPoint()
    {
        target = patrolPoint[index].position;
        nav.SetDestination(target);

    }


    private void incrementPoint()
    {

        if (index != patrolPoint.Count - 1)
        {
            index++;

        }
    }
}
