using TMPro;
using UnityEngine;

public class WaitWave : MonoBehaviour
{
    [Header("Reference")]
    [SerializeField] private TextMeshProUGUI TimerText;



    private void Start()
    {
        GameManager.Instance.WaitWave = this;
        GameManager.Instance.AddTurret = true;
    }
    private void Update()
    {
        if (GameManager.Instance.StartWave)
        {
            GameManager.Instance.TimerPreWave -= Time.deltaTime;
            TimerText.text = string.Format("{0:00}", GameManager.Instance.TimerPreWave);

        }

    }
}
