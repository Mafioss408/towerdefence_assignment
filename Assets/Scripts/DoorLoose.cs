using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorLoose : MonoBehaviour, TakeDamage
{

    [Header("Variable")]
    [SerializeField] private float health;
    [SerializeField] private float maxHealth;

    public float Health { get { return health; } set { value = health; } }
    public float MaxHealth { get => maxHealth; set => value = maxHealth; }



    private HPBar bar;

    private void Start()
    {
        GameManager.Instance.DoorLoose = this;
        health = maxHealth;
        bar = GetComponentInChildren<HPBar>();

    }
    private void Update()
    {

        bar.HpBar(health / maxHealth);

    }


    public void TakeDamage(float damage)
    {

        health -= damage;
        if (health <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        SceneManager.LoadScene(2);
    }

}
