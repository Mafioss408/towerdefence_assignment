using UnityEngine;
using UnityEngine.UI;

public class HPBar : MonoBehaviour
{
    [Header("Reference")]
    [SerializeField] private Image fillBar;


    private void Awake()
    {
        fillBar =gameObject.transform.GetChild(1).GetComponent<Image>();
    }

    public void HpBar(float hp)
    {
        fillBar.fillAmount = hp;
    }

}
