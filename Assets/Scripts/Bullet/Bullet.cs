using System.Collections;
using UnityEngine;

public abstract class Bullet : MonoBehaviour
{

    [SerializeField] private float speed;

    protected float damage;

    protected virtual void Start()
    {
        damage = gameObject.GetComponentInParent<Shoot>().Damage;
    }

    protected virtual void move()
    {
        transform.position += (transform.forward * speed * Time.deltaTime);
        StartCoroutine(Destroy());
    }



    protected virtual IEnumerator Destroy()
    {
        yield return new WaitForSeconds(4f);
        Destroy(gameObject);
    }

}
