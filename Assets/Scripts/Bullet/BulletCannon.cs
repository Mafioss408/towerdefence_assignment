using UnityEngine;

public class BulletCannon : Bullet
{
    [Header("Variable")]
    [SerializeField] private LayerMask ground;
    [SerializeField] private float radiusSphere;


    private void Update()
    {
        base.move();
    }


    private void FixedUpdate()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, 2f, ground))
        {
            Collider[] enemyhit = Physics.OverlapSphere(hit.point, radiusSphere);
            foreach (var item in enemyhit)
            {
                if (item.gameObject.TryGetComponent(out Enemy enemy))
                {
                    enemy.TakeDamage(base.damage);
                    Destroy(gameObject);
                }
            }

        }
    }
}
