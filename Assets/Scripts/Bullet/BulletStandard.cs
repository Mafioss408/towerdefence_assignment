using UnityEngine;

public class BulletStandard : Bullet
{

    private void Update()
    {
        base.move();
    }

    protected void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.TryGetComponent(out Enemy enemy))
        {

            enemy.TakeDamage(base.damage);
            Destroy(gameObject);

        }

    }
}
