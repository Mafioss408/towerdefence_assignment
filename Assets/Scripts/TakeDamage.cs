
public interface TakeDamage
{
    public float Health { get; set; }
    public float MaxHealth { get; set; }


    public void TakeDamage(float damage);

    public void Die();
 
}
